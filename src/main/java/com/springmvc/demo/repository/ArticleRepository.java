package com.springmvc.demo.repository;

import com.springmvc.demo.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepository {
    public List<Article> articles = new ArrayList<>();
    {
        articles.add(new Article(1, "Spring Boot", "Spring Boot completed Guide.", "2a3a93ec-acb9-4a95-ba30-4588e0164307.gif"));
        articles.add(new Article(2, "JavaScript", "JavaScript completed Guide.", "5448a6fc-82eb-4e44-997c-84adb12aa99f.gif"));
        articles.add(new Article(3, "React Js", "React Js completed Guide.", "8e9cda40-8680-4942-be67-d1e9a2609114.gif"));
        articles.add(new Article(4, "Node Js", "Node Js completed Guide.", "0e8ba0cd-c0c3-4885-a3a9-e290035ceaa7.gif"));
        articles.add(new Article(5, "HTML 5", "HTML5 completed Guide.", "4f5bb08a-e79e-46a4-9e05-aa30a756d0b0.gif"));
        articles.add(new Article(6, "CSS 3", "CSS3 completed Guide.", "7a1bc91c-9379-4273-94a0-9163f1d6811d.gif"));
    }

    public List<Article> listAll() {
        return articles;
    }

    public void add(Article article) {
        articles.add(article);
    }

    public Article findById(int id) {
        Article article = articles.stream().filter(item -> item.getId() == id ).findAny().get();
        return article;
    }

    public void update(Article updateArticle, Article article){
        article.setTitle(updateArticle.getTitle());
        article.setDescription(updateArticle.getDescription());
        article.setImg(updateArticle.getImg());
    }

    public void delete(int id){
        Article article = findById(id);
        articles.remove(article);
    }
}
