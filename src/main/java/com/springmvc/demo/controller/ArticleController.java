package com.springmvc.demo.controller;

import com.springmvc.demo.model.Article;
import com.springmvc.demo.service.ArticleService;
import com.springmvc.demo.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ArticleController {
    ArticleService service;

    @Autowired
    FileUploadService fileUploadService;

    @Autowired
    public ArticleController(ArticleService service) {
        this.service = service;
    }

    @Value("${file.imageUrl}")
    String imgUrl;

    @GetMapping("/")
    public String homePage(Model model){
        List<Article> articles = service.listAll();
        model.addAttribute("articles", articles);
        return "index";
    }

    @RequestMapping("/findById")
    @ResponseBody
    public Article findById(@PathVariable int id) {
        System.out.println(service.findById(id));
        return service.findById(id);
    }

    @PostMapping(value = "/add")
    public String addArticle(@ModelAttribute Article article, @RequestPart("file") MultipartFile file) {
        String fileName = fileUploadService.fileUpload(file);
        article.setId(service.listAll().get(service.listAll().size()-1).getId() + 1);
        article.setImg(fileName);
        service.add(article);
        System.out.println(article.getImg());
        System.out.println("image Url : " + fileName);
        return "redirect:/";
    }

    @RequestMapping(value="/update", method = {RequestMethod.POST, RequestMethod.GET})
    public String update(@ModelAttribute Article updateArticle, int id,  @RequestPart("updateFile") MultipartFile updateFile) {
        Article article = findById(id);
        if (updateFile.isEmpty()){
            updateArticle.setImg(article.getImg());
        } else {
            String updateFileName = fileUploadService.fileUpload(updateFile);
            updateArticle.setImg(updateFileName);
        }
        service.update(updateArticle, article);
        return "redirect:/";
    }

    @RequestMapping(value="/delete", method = {RequestMethod.DELETE, RequestMethod.GET})
    public String delete(int id) {
        service.delete(id);
        return "redirect:/";
    }
}
