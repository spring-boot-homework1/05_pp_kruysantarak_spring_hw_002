package com.springmvc.demo.service;

import com.springmvc.demo.model.Article;
import com.springmvc.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class ArticleService implements ArticleServiceInterface{

    @Value("${file.server.path}")
    String serverPath;

    @Value("${file.noImage}")
    String noImage;

    ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> listAll() {
        return articleRepository.listAll();
    }

    @Override
    public void add(Article article) {
        articleRepository.add(article);
    }

    @Override
    public Article findById(int id) {
        return articleRepository.findById(id);
    }

    @Override
    public void update(Article updateArticle, Article article) {
        articleRepository.update(updateArticle, article);
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

//    @Override
//    public String fileUpload(MultipartFile file) {
//        String afterSavedFilename = noImage;
//        if (!file.isEmpty()) {
//            try {
//                String filename = file.getOriginalFilename();
//                assert filename != null;
//                afterSavedFilename = UUID.randomUUID().toString() + "." + filename.substring(filename.lastIndexOf(".") + 1);
//                Files.copy(file.getInputStream(), Paths.get(serverPath, afterSavedFilename));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return afterSavedFilename;
//    }
}
