package com.springmvc.demo.service;

import com.springmvc.demo.model.Article;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ArticleServiceInterface {
    List<Article> listAll();
    void add(Article article);
    Article findById(int id);
    void update(Article newArticle, Article oldArticle);
    void delete(int id);
//    String fileUpload(MultipartFile file);
}
